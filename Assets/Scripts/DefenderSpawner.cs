﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class DefenderSpawner : MonoBehaviour
{
    private Defender defender;
    private GameObject defenderParent;
    private const string DEFENDER_PARENT_NAME = "Defenders";
    private StarDisplay starDisplay;

    private void Start()
    {
        starDisplay = FindObjectOfType<StarDisplay>();
        CreateDefenderParent();
    }

    private void CreateDefenderParent()
    {
        defenderParent = GameObject.Find(DEFENDER_PARENT_NAME);
        if (!defenderParent)
        {
            defenderParent = new GameObject(DEFENDER_PARENT_NAME);
        }
    }

    private void OnMouseDown()
    {
        SpawnDefender();
    }

    public void SetSelectedDefender(Defender defender)
    {
        this.defender = defender;
    }
    
    private void SpawnDefender()
    {
        if(!defender) return;
        if(starDisplay.Stars < defender.StarCost) return;
        Defender newDefender = Instantiate(defender, GetSquareClicked(), Quaternion.identity);
        newDefender.transform.parent = defenderParent.transform;
        starDisplay.SpendStars(defender.StarCost);
    }

    private Vector2 GetSquareClicked()
    {
        Vector2 clickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(clickPos);
        return SnapToGrid(worldPos);
    }

    private Vector2 SnapToGrid(Vector2 rawWorldPos)
    {
        float newX = Mathf.RoundToInt(rawWorldPos.x);
        float newY = Mathf.RoundToInt(rawWorldPos.y);
        return new Vector2(newX, newY);
    }
}
