﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private float timeToWaitOnTheSplashScreen = 2f;
    private int currentSceneIndex;
    
    // Start is called before the first frame update
    void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0)
        {
            StartCoroutine(QuitSplashScreen());
        }
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene("Start Screen");
    }

    public void LoadOptionScreen()
    {
        SceneManager.LoadScene("Option Screen");
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator QuitSplashScreen()
    {
        yield return new WaitForSeconds(timeToWaitOnTheSplashScreen);
        LoadStartScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
