﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float rotationSpeed = 20f;
    private float newXPos;

    private void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //newXPos = transform.position.x + moveSpeed * Time.deltaTime;
        //transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
        transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
    }
}
