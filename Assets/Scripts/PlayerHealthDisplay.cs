﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealthDisplay : MonoBehaviour
{
    private int playerHealth;
    private Text healthText;
    
    [Header("Game Over Settings")]
    [SerializeField] private AudioClip gameOverSound;
    [SerializeField] [Range(0,2)] private float gameOverSoundVolume = 1f;
    [SerializeField] private float delayInSeconds = 1f;

    
    // Start is called before the first frame update
    void Start()
    {
        playerHealth = Mathf.FloorToInt((1 - PlayerPrefsController.GetDifficulty()) * 10);
        if (playerHealth == 0) playerHealth = 1;
        healthText = GetComponent<Text>();
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        healthText.text = playerHealth.ToString();
    }

    public void LoseHealth()
    {
        playerHealth--;
        if (playerHealth == 0)
        {
            Lose();
        }
        UpdateDisplay();
    }

    public int PlayerHealth => playerHealth;

    public void Lose()
    {
        AudioSource.PlayClipAtPoint(gameOverSound, Camera.main.transform.position, gameOverSoundVolume);
        StartCoroutine(GameOver());
    }

    IEnumerator GameOver()
    {
        yield return new WaitForSeconds(delayInSeconds);
        SceneManager.LoadScene("Game Over");
    }
}
