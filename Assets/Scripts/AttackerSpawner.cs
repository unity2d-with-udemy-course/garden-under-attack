﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AttackerSpawner : MonoBehaviour
{
    [SerializeField] private Attacker[] attackers;
    [SerializeField] private float maxTimeBetweenTwoSpawns = 5f;
    [SerializeField] private float minTimeBetweenTwoSpawns = 1f;
    
    private bool spawn = true;
    private float randomTime;
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (spawn)
        {
            randomTime = Random.Range(minTimeBetweenTwoSpawns, maxTimeBetweenTwoSpawns);
            yield return new WaitForSeconds(randomTime);
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        var randomIndex = Random.Range(0, attackers.Length);
        Spawn(attackers[randomIndex]);
    }

    private void Spawn(Attacker attacker)
    {
        Attacker newAttacker = Instantiate(attacker, transform.position, Quaternion.identity);
        newAttacker.transform.parent = transform;
    }

    public void StopSpawning()
    {
        spawn = false;
    }
}
