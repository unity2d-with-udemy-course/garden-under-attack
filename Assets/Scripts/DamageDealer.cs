﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] private int damage = 50;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var attacker = other.GetComponent<Attacker>();
        if (attacker)
        {
            Hit();
        }
    }

    public void Hit()
    {
        Destroy(gameObject);
    }

    public int Damage => damage;
}
