﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [Header("Game Over Settings")]
    [SerializeField] private AudioClip winSound;
    [SerializeField] [Range(0,2)] private float winSoundVolume = 1f;
    [SerializeField] private float delayInSeconds = 1f;
    [SerializeField] private GameObject winLabel;

    private int numberOfAttackers = 0;
    private bool levelTimerFinished = false;

    private void Start()
    {
        winLabel.SetActive(false);
    }

    public void AttackerSpawned()
    {
        numberOfAttackers++;
    }

    public void AttackerDied()
    {
        numberOfAttackers--;
        if (numberOfAttackers <= 0 && levelTimerFinished)
        {
            StartCoroutine(HandleWinCondition());
        }
    }

    private IEnumerator HandleWinCondition()
    {
        winLabel.SetActive(true);
        // si on met un composent AudioSource au LevelController il faut faire :
        // GetComponent<AudioSource>().Play();
        // sinon on fait :
        AudioSource.PlayClipAtPoint(winSound, Camera.main.transform.position, winSoundVolume);
        yield return new WaitForSeconds(delayInSeconds);
        FindObjectOfType<LevelLoader>().LoadNextScene();
    }

    public void LevelTimerFinished()
    {
        levelTimerFinished = true;
        StopSpawners();
    }

    private void StopSpawners()
    {
        AttackerSpawner[] attackerSpawners = FindObjectsOfType<AttackerSpawner>();
        foreach (var attackerSpawner in attackerSpawners)
        {
            attackerSpawner.StopSpawning();
        }
    }
}
