﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenderButton : MonoBehaviour
{
    [SerializeField] private Defender defenderPrefab;

    private void Start()
    {
        LabelButtonWithCost();
    }

    private void LabelButtonWithCost()
    {
        Text costLabel = GetComponentInChildren<Text>();
        if (!costLabel)
        {
            Debug.LogError(name + " has no cost text ?");
        }
        else
        {
            costLabel.text = defenderPrefab.StarCost.ToString();
        }
    }

    private void OnMouseDown()
    {
        var defenders = FindObjectsOfType<DefenderButton>();
        Color initialColor = GetComponent<SpriteRenderer>().color;
        foreach (DefenderButton defender in defenders)
        {
            defender.GetComponent<SpriteRenderer>().color = initialColor;
        }
        GetComponent<SpriteRenderer>().color = Color.white;
        FindObjectOfType<DefenderSpawner>().SetSelectedDefender(defenderPrefab);
    }
}
