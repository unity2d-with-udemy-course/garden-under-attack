﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCollider : MonoBehaviour
{
    private PlayerHealthDisplay playerHealthDisplay;
    
    // Start is called before the first frame update
    void Start()
    {
        playerHealthDisplay = FindObjectOfType<PlayerHealthDisplay>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        playerHealthDisplay.LoseHealth();
        Destroy(other.gameObject);
    }
}
