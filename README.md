A game of type "Plants VS Zombies" done with a Udemy class.


Sources :

Font of the title : Top Secret by Koczman Bálint (Magique Fonts)

Sound effects and sprites from Kenney's web site : https://kenney.nl/

Background start menu by WenPhotos on pixabay

Background Game Over Screen by ojkumena on pixabay

Game Over sound by myfox14 on freesound.org

Background of the levels : Chris of the website https://2dgameartguru.com/

Sprites and animations from the opensource game Glitch : https://www.glitchthegame.com/